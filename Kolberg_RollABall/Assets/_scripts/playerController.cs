﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public float speed;
    public Text countText;
    public Text winText;
    public bool cubeIsOnGround = true;

    private Rigidbody rb;
    private int count;

    public GameObject WallToDestroy;
    // Start is called before the first frame update
    void Start()
    {
        //Set up reference for Rigid body
        rb = GetComponent<Rigidbody>();
        //Set default values for game and UI text
        count = 0;
        SetCountText();
        winText.text = "";
    }

    // Update is called once per frame


    void FixedUpdate()
    {
        //Get the input on each axis
        float moveHorizontal = Input.GetAxis ("Horizontal");
        float moveVertical = Input.GetAxis ("Vertical");

        Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
        //Apply movement to actual ball
        rb.AddForce(movement * speed);

        //added in code to allow player to jump
        if (Input.GetButtonDown("Jump") && cubeIsOnGround) { 
            rb.AddForce(new Vector3(0, 5, 0), ForceMode.Impulse);
            cubeIsOnGround = false;
        }
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Floor") { cubeIsOnGround = true; }
    } //Set tags on both playing areas to allow the player to jump on both levels

    void OnTriggerEnter(Collider other)
    {
        //Upon collision, pickups are deactivated and the count goes up
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            //After 12 pickups are collected, north wall is destroyed to open the new level
            if (count == 12) { Destroy(WallToDestroy); }
            SetCountText();

        }
    }
    void SetCountText()
    {
        //Established count text and win condition
        countText.text = "Count: " + count.ToString();
        if (count >= 21)
        {
            winText.text = "You Win!";
        }
    }
}



